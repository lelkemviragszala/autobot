from automagica import *
import cv2 as cv
import numpy as np
import imutils
import pyautogui
import cv2
'''
pyautogui.screenshot("screenshot.png")
# we can then load our screenshot from disk in OpenCV format
image = cv2.imread("screenshot.png")
#cv2.imshow("Screenshot", imutils.resize(image, width=600))
#cv2.waitKey(0)

from matplotlib import pyplot as plt

# img_rgb = cv2.imread('mario.png')
img_rgb = image
img_gray = cv2.cvtColor(img_rgb, cv.COLOR_BGR2GRAY)
template = cv2.imread('capture.png', 0)
w, h = template.shape[::-1]
res = cv2.matchTemplate(img_gray, template, cv.TM_CCOEFF_NORMED)
threshold = 0.9
loc = np.where(res >= threshold)

matches = []

for pt in zip(*loc[::-1]):
    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 0, 255), 2)
    matches.append([pt, pt[0] + w, pt[1] + h])
cv2.imwrite('res.png', img_rgb)
#cv2.imshow("Screenshot", imutils.resize(img_rgb, width=600))
#cv2.waitKey(0)
x1, y1, x2, y2 = 0, 0, 0, 0
tresh = 5
for m in matches:
    if x1 - tresh <= m[0][0] <= x1 + tresh and y1 - tresh <= m[0][1] <= y1 + tresh:
        continue
    else:
        print("%s %d %d" % (m[0], m[1], m[2]))
        x1, y1 = m[0]
        x2, y2 = m[1], m[2]
'''
import pytesseract
from pytesseract import Output
from PIL import Image
import argparse

pyautogui.screenshot("screen.png")
# we can then load our screenshot from disk in OpenCV format
img = cv2.imread("screen.png")
scale_percent = 200 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
#gray = cv2.bitwise_not(gray)


#gray = cv2.threshold(gray, 0, 255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
gray = cv2.medianBlur(gray, 1)

filename = "{}.png".format(os.getpid())
cv2.imwrite(filename, gray)


d = pytesseract.image_to_data(Image.open(filename), output_type=Output.DICT)
n_boxes = len(d['level'])
for i in range(n_boxes):
    (x, y, w, h) = (d['left'][i], d['top'][i], d['width'][i], d['height'][i])
    cv2.rectangle(resized, (x, y), (x + w, y + h), (0, 255, 0), 2)

cv2.imwrite('resized.png',resized)
#cv2.waitKey(0)
